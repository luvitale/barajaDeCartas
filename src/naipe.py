'''
Created on 07/10/2020

@author: LuVitale
'''

class Naipe(object):
    '''
    classdocs
    '''
    
    def getNumeros(self):
        MIN = 1
        MAX = 12
        return range(MIN, MAX+1)
    
    def getFiguras(self):
        return ["Espada", "Basto", "Oro", "Copa"]

    def __str__(self):
        return str(self.__numero) + " de " + self.__figura
    
    def __eq__(self, other):
        return str(self) == str(other)

    def __init__(self, *args, **kwargs):
        '''
        Constructor
        '''
        self._numero = self.getNumeros()[0]
        self._figura = self.getFiguras()[0]
        if(args):
            numero = args[0]
            figura = args[1]
        else:
            numero = kwargs["Numero"]
            figura = kwargs["Figura"]
        
        if not self.getNumeros().__contains__(numero):
            raise(Exception, "Se debe ingresar un número válido")
        self.__numero = numero
        
        if not self.getFiguras().__contains__(figura):
            raise(Exception, "Se debe ingresar una figura válida")
        self.__figura = figura
        