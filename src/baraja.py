'''
Created on 07/10/2020

@author: LuVitale
'''

from src.naipe import Naipe
from random import shuffle

class Baraja(object):
    '''
    classdocs
    '''

    def entregarNaipe(self, inferior = False):
        if inferior:
            return self.__naipes.pop(0)
        else:
            return self.__naipes.pop()
        
    def naipeIncluido(self, naipe):
        return self.__naipes.__contains__(naipe)

    def retornarNaipe(self, naipe):
        if self.naipeIncluido(naipe):
            return False
        
        self.__naipes.append(naipe)
        return True

    def estanTodosLosNaipes(self):
        MAXCANTNAIPES = 48
        return len(self.__naipes) == MAXCANTNAIPES
            
    def mezclar(self):
        if self.estanTodosLosNaipes():
            shuffle(self.__naipes)
            return True
        
        return False

    def __str__(self, *args, **kwargs):
        naipes = ""
        for naipe in self.__naipes:
            naipes += str(naipe) + "\n"
        return naipes

    def __init__(self, params):
        '''
        Constructor
        '''
        self.__naipes = list()
        for figura in Naipe.getFiguras(self):
            for numero in Naipe.getNumeros(self):
                self.__naipes.append(Naipe(numero, figura))