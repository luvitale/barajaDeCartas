'''
Created on 07/10/2020

@author: LuVitale
'''
import unittest
from src.naipe import Naipe


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def test7DeEspadaStr(self):
        NUMERO = 7
        FIGURA = "Espada"
        NAIPE = Naipe(NUMERO, FIGURA)
        
        ESPERADO = str(NUMERO) + " de " + FIGURA
        RESULTADO = str(NAIPE)
        
        self.assertEqual(ESPERADO, RESULTADO)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()