'''
Created on 07/10/2020

@author: LuVitale
'''
import unittest
from src.baraja import Baraja
from src.naipe import Naipe


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testTodaLaBaraja(self):
        naipes = ""
        for figura in Naipe.getFiguras(self):
            for numero in Naipe.getNumeros(self):
                naipes += str(numero) + " de " + figura + "\n"
        
        baraja = Baraja(self)
        
        ESPERADO = naipes
        RESULTADO = str(baraja)
        
        self.assertEqual(ESPERADO, RESULTADO)
        
    def testEntregarNaipeSuperior12DeCopa(self):
        baraja = Baraja(self)
        RESULTADO = str(baraja.entregarNaipe())
        
        naipe = Naipe(12, "Copa")
        ESPERADO = str(naipe)
        
        self.assertEqual(ESPERADO, RESULTADO)
    
    def testEntregarNaipeInferior1DeEspada(self):
        baraja = Baraja(self)
        RESULTADO = str(baraja.entregarNaipe(True))
        
        naipe = Naipe(1, "Espada")
        ESPERADO = str(naipe)
        
        self.assertEqual(ESPERADO, RESULTADO)
        
    def testEntregarNaipesDeCopaYRetornar7DeCopa(self):
        baraja = Baraja(self)
        for i in range(0, 12):
            baraja.entregarNaipe()
        
        naipe = Naipe(7, "Copa")
        
        baraja.retornarNaipe(naipe)
        
        RESULTADO = str(baraja)
        
        naipes = ""
        for figura in Naipe.getFiguras(self):
            if figura != "Copa":
                for numero in Naipe.getNumeros(self):
                    naipes += str(numero) + " de " + figura + "\n"
            else:
                naipes += "7 de Copa\n"
                
        ESPERADO = naipes
        
        self.assertEqual(ESPERADO, RESULTADO)
        
    def testNoRetornar7DeCopaEnBarajaCompleta(self):
        baraja = Baraja(self)
        
        naipe = Naipe(7, "Copa")
        
        RESULTADO = baraja.retornarNaipe(naipe)
        
        self.assertEqual(False, RESULTADO)
        
    def testMezclarConTodosLosNaipes(self):
        baraja = Baraja(self)
        RESULTADO = baraja.mezclar()
        
        self.assertEqual(True, RESULTADO)
        
    def testNoMezclarHabiendoEntregado1Naipe(self):
        baraja = Baraja(self)
        baraja.entregarNaipe()
        RESULTADO = baraja.mezclar()
        
        self.assertEqual(False, RESULTADO)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()